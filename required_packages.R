pkgLoad <- function( packages = "favourites" ) {

  if( length( packages ) == 1L && packages == "favourites" ) {
    packages <- c("ade4", "ape", "BiocManager", "data.table", "DECIPHER", "dplyr", "DT", "ggallin", "ggfortify", "ggplot2", "ggpubr", "ggridges", "multcomp", "plotly", "readr", "Rsamtools", "rstudioapi", "tibble", "tidyverse"
    )
  }

  packagecheck <- match( packages, utils::installed.packages()[,1] )

  packagestoinstall <- packages[ is.na( packagecheck ) ]

  if( length( packagestoinstall ) > 0L ) {
    utils::install.packages( packagestoinstall,
                             repos = "http://cran.csiro.au"
    )
  } else {
    print( "All requested packages already installed" )
  }

  for( package in packages ) {
    suppressPackageStartupMessages(
      library( package, character.only = TRUE, quietly = TRUE )
    )
  }

}
