# TE_Reg_Repository

---------------------------------------------------

README file: Regulatory divergence between gene duplicates is associated with transposable element derived cis-regulatory elements in liver

Description of project:
Transposable elements are estimated to play important roles the history of genome evolution. A recent analysis revealed that duplicate gene copies which had higher expression in liver following the salmonid whole genome duplication 100 mya~ were coinciding with a higher number of predicted TE-derived cis-regulatory elements. However, the ability of these TE CREs to recruit transcription factors and impact gene expression in vivo is unknown. This project evaluates 11 TEs using luciferase promoter reporter assays in salmon primary liver cells. 

This project has two goals. 1. To determine the last transposition event of each TE, and whether this was before or after the salmonid WGD event. 2. To statistically analyse luciferase reporter assay data, and evaluate if the TEs may have an impact on gene expression. This is done by normalizing the output values from luciferase gene expression (the test variable being effected by TE regulation) by dividing this by a control gene expression (renilla). This reduces the impact of confounding variables such as varying cell count. These results are visualized in bar plots beside a positive control (CMV), and p-values are calculated by performing a regression analysis (lm) for the Non-Mariner dataset, and a Tukey test for the Tc1/Mariner dataset. This is due to that the Tc1/Mariner dataset is not a repeat of the same experiment three times, as is the case with the non/Mariner dataset. Instead, the Tc1/Mariner dataset is three experiments testing different sections of the TE to try to identify the specific motifs/locations involved in up-regulation. This project gitlab also contains a folder with supplementary data files to this article DOIXXXXXXX. 

There are seven R scripts in this project. They are named:
- New_candidates_script.Rmd
- Phylogenetic_Analysis.Rmd
- Ridgeplot_Dissimilarity_Pipeline.Rmd
- TE_Tc1_Experiments_Analysis.Rmd
- TE_NonMariners_Experiments_Analysis.Rmd
- blastsearch.sh
- required_packages.R

`New_canddidates_script.Rmd` eliminates TEs that are not of interest for wet-lab experimentation. From the dataset this process excludes Tc1-Mariner TEs and Tes shorter than 100bp. The remaining TEs were included in the Non-Mariner Experiment. 

`Phylogenetic_Analysis.Rmd` performs a phylogenetic analysis of the initial dataset of TEs from Gillard et al, and lines them up with consensus sequences from Lien et al. Here we include only Tc1-Mariners, and plot the sequences in a tree using ggtree. Following this we make pair-wise alignments and only include TEs over 100bp. This gives us a final list of 5 TEs that are viable for wet-lab experimentation. 

`Ridgeplot_Dissimilarity_Pipeline.Rmd` determines the "age" of the TEs. This is done by calculating the similarity, or dissimilarity of the DNA sequences to other similar elements. The more dissimilar the TE is from other similar elements, the older it is estimated to be. 


`TE_Tc1_Experiments_Analysis.Rmd` normalizes the luciferase reporter assay results from the Tc1/Mariner TE experiments and returns a bar plot of the normalized data, as well as p-values from a Tukey test of the data compared to the positive control sample (CMV). 

`TE_NonMariners_Experiments_Analysis.Rmd` normalizes the luciferase reporter assay results from the Non-Mariner TE experiments and returns a bar plot of the normalized data, as well as p-values from a linear model regression analysis.

`blastsearch.sh` does similarity searches between specific TE insertions and the RepBase database, and between the consensus sequences annotating those insertions and the RepBase database, for purposes of curation and classification.

`required_packages.R` contains a function that installs all needed packages if not already installed. This script is called and run in the beginning of each R script. 

There are three folders in this repository:
- Supplementary_Files
- input_data
- results_folder 

`Supplementary_Files` contains xlxs files describing the transposable elements, as well as sequences for PCR primers, and DNA vector maps for each experimental construct. If any information is not included that is of interest please contact (Hanna Sahlström?). 

`input_data` includes all input files required for the R scripts to run, including raw luciferase assay data, fasta sequences for all TEs, and consensus sequence data. 

`results_folder` includes all resulting plots from R scripts, including bar plots of luciferase assay analysis, phylogenetic tree and age plot. There are also fasta files of the relevant TEs, consensus alignments and the final TE lists. 



